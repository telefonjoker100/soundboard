import os
import sys
import time

def get_absolut_path_main():
    curr_dir = os.path.abspath(os.path.dirname(sys.argv[0]))
    relativ_path_main = 'Packages/main.py'
    absolut_path_main = os.path.join(curr_dir, relativ_path_main)
    return absolut_path_main

def get_absolut_path_log():
    curr_dir = os.path.abspath(os.path.dirname(sys.argv[0]))
    relativ_path_log = 'log.txt'
    absolut_path_log = os.path.join(curr_dir, relativ_path_log)
    return absolut_path_log

def wait_for_usb_found(path):
    start_dir = os.path.abspath(os.path.dirname(sys.argv[0]))
    print('start_dir', start_dir)
    os.chdir(path)
    while len(os.listdir(path)) == 0:
        print('No USB-Drive found')
        time.sleep(1)
        pass
    print('USB-Drive found')

def get_main_action():
    absolut_path_main = get_absolut_path_main()
    absolut_path_log = get_absolut_path_log()
    action = ' '.join(['sudo python3', absolut_path_main, '>>', absolut_path_log])
    return action

def get_copy_log_action(path):
    folder = os.path.join(path, os.listdir()[0])
    os.chdir(folder)
    file_target = get_absolut_path_log()
    file_destination = os.path.join(folder, 'log.txt')
    action = ' '.join(['cp', file_target, file_destination])
    return action

def copy_log():
    path = '/media/pi'
    wait_for_usb_found(path)
    action = get_copy_log_action(path)
    print(action)
    os.system(action)
    print('Config copied')
    time.sleep(1)

def run_main():
    os.system(get_main_action())

if __name__ == '__main__':
    copy_log()
    run_main()