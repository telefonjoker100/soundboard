import os
import sys
import time
from dotenv import load_dotenv

try:
    from Packages.EXTRA.shutdown import reboot
    from Packages.SOUNDBOARD.soundboard import soundboard
except ModuleNotFoundError or ImportError:
    from EXTRA.shutdown import reboot
    from SOUNDBOARD.soundboard import soundboard

def wait_for_usb_found():
    path = '/media/pi'
    start_dir = get_start_dir()
    os.chdir(path)
    while len(os.listdir(path)) == 0:
        print('No USB-Drive found')
        time.sleep(1)
        pass
    print('USB-Drive found')

def copy_env():
    action = get_copy_env_action()
    os.system(action)
    print('Config copied')
    time.sleep(1)

def get_copy_env_action():
    file_target = get_env_file_target()
    file_destination = get_env_file_destination()
    action = ' '.join(['cp', file_target, file_destination])
    return action

def get_env_file_target():
    folder = get_usb_path()
    file_target = os.path.join(folder, '.env')
    return file_target

def get_env_file_destination():
    main_path = os.path.abspath(os.path.dirname(sys.argv[0]))
    path_split = main_path.split('/')
    env_folder = '/'.join(path_split[:-1])
    env_path = os.path.join(env_folder, '.env')
    return env_path


def get_usb_path():
    path = '/media/pi'
    os.chdir(path)
    folder = os.path.join(path, os.listdir()[0])
    return folder

def get_start_dir():
    start_dir = os.path.abspath(os.path.dirname(sys.argv[0]))
    return start_dir

def start_soundboard():
    folder = get_usb_path()
    sb = soundboard(folder)

def main():
    wait_for_usb_found()
    copy_env()
    start_soundboard()




main()
while True:
    pass
