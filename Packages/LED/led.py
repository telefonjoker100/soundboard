import board
import neopixel
import time
from threading import Thread
import decimal

class led_pixels():
    def __init__(self, pixel_pin, num_pixels, brightness=0.2, fade_time=0.25, enabled=True):
        self.pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness=brightness, auto_write=False, pixel_order=neopixel.GRB)
        self.color_samples = {'background': (0,0,0)}
        self.rgb_fade_array = (0,0,0)
        self.num_steps = 10
        self.time_to_fade = fade_time
        self.enabled = enabled

    def show(self):
        if self.enabled:
            self.pixels.show()

    def set_color_samples(self, data_dict):
        self.color_samples = data_dict

    def add_color_samples(self, key, value):
        self.color_samples[key] = value

    def remove_color_from_color_samples(self, key):
        self.color_samples.pop(key, None)

    def show_color_samples_from_list(self, led_list):
        assert len(self.pixels) >= len(led_list)
        for i in range(0, len(led_list)):
            self.pixels[i] = self.color_samples[led_list[i]]
        self.show()

    def _calculate_fade(self, start_rgb, target_rgb, num_steps=None):
        if num_steps is None:
            num_steps = self.num_steps

        [start_r, start_g, start_b] = start_rgb
        [target_r, target_g, target_b] = target_rgb
        
        r_fade = self.__make_list(start_r, target_r, num_steps)
        g_fade = self.__make_list(start_g, target_g, num_steps)
        b_fade = self.__make_list(start_b, target_b, num_steps)
        rgb_fade_array = []
        for i in range(0, num_steps):
            rgb_fade_array.append((r_fade[i], g_fade[i], b_fade[i]))
        rgb_fade_array.append((target_r, target_g, target_b))
        return rgb_fade_array
    
    def __make_list(self, start, target, num_steps):
        step_width = (target-start)/num_steps
        flist = self.__frange(start, target, step_width)
        ilist = []
        if flist is None:
            for i in range(0,num_steps):
                ilist.append(int(start))
        else:
            for float_value in flist:
                ilist.append(int(float_value))
        return ilist
    
    def __frange(self, x, y, jump):
        alist = []
        if x < y:
            while x < y:
                alist.append(float(x))
                x += decimal.Decimal(jump)
            return alist
        elif x > y:
            while x > y:
                alist.append(float(x))
                x += decimal.Decimal(jump)
            return alist
        else:
            return None

    def fade(self, pixels):
        print(time.thread_time())
        self.fade_on(pixels)
        self.fade_off(pixels)
        print(time.thread_time())
    
    def fade_on(self, rgb_fade_array, pixels, time_to_fade=None):
        sleep_time = self.__calc_sleep_time(time_to_fade)
        self.fade_rgb_array(pixels, rgb_fade_array, sleep_time)
   
    def fade_off(self, pixels, time_to_fade=None):
        sleep_time = self.__calc_sleep_time(time_to_fade)
        rgb_fade_array_backwards = self.rgb_fade_array[::-1]
        self.fade_rgb_array(pixels, rgb_fade_array_backwards, sleep_time)
    
    def __calc_sleep_time(self, time_to_fade=None):
        if time_to_fade is None:
            time_to_fade = self.time_to_fade
        sleep_time = time_to_fade/self.num_steps
        return sleep_time
    
    def fade_rgb_array(self, pixels, rgb_array, sleep_time):
        for rgb in rgb_array:
            start_time = time.thread_time()
            self.__timer(start_time, sleep_time)
            for pixel in pixels:
                self.pixels[pixel] = rgb
                self.show()
    
    def fade_off_after_time(self, pixels, sleep_time):
        print('fade_off_thread start ')
        print(time.time())
        start_time = time.thread_time()
        self.__timer(start_time, sleep_time)
        self.fade_off(pixels)
        print('fade_off_thread ende')
        print(time.time())

    def __timer(self, start_time, sleep_time):
        target_time = start_time + sleep_time
        while time.thread_time() < target_time:
            pass
        
    
    

if __name__ == "__main__":
    led = led_pixels(board.D21, 18)
    color_samples_dict = {'no_sounds_color': (0, 0, 0), 'some_sounds_color': (100, 100, 0), 'all_sounds_color': (100, 0, 0)}
    led.set_color_samples(color_samples_dict)
    a_list = []
    for i in range(0,18):
        a_list.append('no_sounds_color')
    a_list[12] = 'some_sounds_color'
    led.show_color_samples_from_list(a_list)

