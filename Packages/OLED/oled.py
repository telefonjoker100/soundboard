from luma.core.interface.serial import i2c
from luma.core.render import canvas
from luma.oled.device import ssd1306
from PIL import ImageFont, ImageDraw, Image



class oled_ssd1306():
    def __init__(self, font_size = 20, font_type = 'FreeSans.ttf', rotation = 0, debug = False):
        self.serial = i2c(port=1, address=0x3c)
        self.device = ssd1306(self.serial, rotate = rotation)
        self.oled_font = ImageFont.truetype(font_type, font_size)
        self.lines = ['', '', '']
        
        self.lines_shown = ['','','']
        if debug:
            self.num_lines_shown = 2
        else:
            self.num_lines_shown = 3
    
    def set_scroll_data(self, lines):
        assert type(lines)==list
        for line in lines:
            assert type(line)==str
        assert len(lines) >= self.num_lines_shown, 'you need more lines. Eventhough only the first 3 will be displayed'
        self.lines = lines
        self.cursor_row = 0
        self.lines_shown[0:self.num_lines_shown] = lines[0:self.num_lines_shown]
        self.show()
        
    def set_cursor_row(self, row):
        if self.cursor_row <= len(self.lines):
            self.cursor_row = row
        self.scroll(0)
            
    def get_cursor_row(self):
        return self.cursor_row
    
    def scroll(self, direction=1):
        self.cursor_row += direction
        if self.cursor_row >= len(self.lines):
            self.cursor_row = 0
        elif self.cursor_row < 0:
            self.cursor_row = len(self.lines)-1
        line_cursor = self.cursor_row
        for i in range (0, self.num_lines_shown):
            if line_cursor >= len(self.lines):
                line_cursor -=  len(self.lines)
            self.lines_shown[i] = self.lines[line_cursor]
            line_cursor += 1
        self.show()
    
    def write_text(self, text):        
        if type(text) == list:
            self.write_line_1(text[0])
            self.write_line_2(text[1])
        elif type(text) == str:
            self.write_line_1(text)
    
    def _set_line_1(self, text):
        self.lines_shown[0] = text
            
    def _set_line_2(self, text):
        self.lines_shown[1] = text
    
    def _set_line_3(self, text):
        self.lines_shown[2] = text
    
    def set_debug_msg(self, text):
        self._set_line_3(text)
            
    def show(self, text=None):
        text = self.__convert_text(text)
        with canvas(self.device) as draw:
            #draw.rectangle(device.bounding_box, outline = "white", fill = "black")
            draw.text((0, 0), text, font = self.oled_font, fill = "white")
    
    def clear(self):
        self.lines_shown = ['','','']
        self.show()
        
    def __convert_text(self, text):
        if text is None:
            return self.__convert_list_to_text(self.lines_shown)
            
        elif type(text) == list:
            assert len(text) <= 3
            self.lines_shown = text
            return self.__convert_list_to_text(self.lines_shown)
        
        elif type(text) == str:
            return text
        
        else:
            assert False, 'text is neither none, list_type or str_type'
            
    def __convert_list_to_text(self, text_list):
        return '\n'.join(text_list)
    
if __name__ == "__main__":
    oled = oled_ssd1306(debug=True)
    scroll_data = ['00 | Test', '01 | ok-1', '02 | ok-2', '03 | ok-3', '04 | let it be']
    oled.set_scroll_data(scroll_data)
    #oled.show()
    oled.set_debug_msg('Time=3')
    while True:
        x = input()
        if x == 'x':
            oled.clear()
        else:
            x = int(x)
            print(x)
            oled.scroll(x)
            print(oled.cursor_row)
            

            
