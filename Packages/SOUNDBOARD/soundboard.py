#V2

#Standard libs
import os
import sys
import glob
import time
import RPi.GPIO as GPIO
import threading

#needed Lib import to support modular_pin_mapping
from adafruit_blinka.microcontroller.bcm283x.pin import Pin as led_pin

#Open Libs
from pydub import AudioSegment
from pydub.playback import play
from dotenv import load_dotenv

#Own Libs
try:
    from Packages.OLED.oled import oled_ssd1306
    from Packages.LED.led import led_pixels
    from Packages.EXTRA.shutdown import shutdown, reboot
    from Packages.SOUNDBOARD.sound_list import sound_list
except ModuleNotFoundError or ImportError:
    from OLED.oled import oled_ssd1306
    from LED.led import led_pixels
    from EXTRA.shutdown import shutdown, reboot
    from SOUNDBOARD.sound_list import sound_list

class soundboard():
    def __init__(self, folder_path):
        load_dotenv()
        self.__instantiate_global_variables()
        self.load_config()
        self.__oled_startup()
        self.__setup(folder_path)
        
        self.__runtime()

    def __instantiate_global_variables(self):
        self.__key1 = 0
        self.__time = 0
        self.num_pixels = 18
        self.__key_arrows = {'u': None, 'd': None}

    def load_config(self):
        color_assignment =   {
                            'no_sounds_color': tuple([int(i) for i in os.environ.get('no_sounds_color').split(',')]),
                            'some_sounds_color': tuple([int(i) for i in os.environ.get('some_sounds_color').split(',')]),
                            'all_sounds_color': tuple([int(i) for i in os.environ.get('all_sounds_color').split(',')]),
                            'play_sound_color': tuple([int(i) for i in os.environ.get('play_sound_color').split(',')]),
                            'boot_loading_color': tuple([int(i) for i in os.environ.get('boot_loading_color').split(',')]),
                            'arrow_color': tuple([int(i) for i in os.environ.get('arrow_color').split(',')]),
                            }

        self.__conf =   {
                        'time_to_select': int(os.environ.get('time_to_select')),
                        'amount_select_keys': int(os.environ.get('amount_select_keys')),
                        'extension_list': (os.environ.get('extension_list')).split(' '),
                        'parallel_sound': bool(int(os.environ.get('parallel_sound'))),
                        'switch_simulation': bool(int(os.environ.get('switch_simulation'))),
                        'led_enabled': bool(int(os.environ.get('led_enabled'))),
                        'debounce_time': int(os.environ.get('debounce_time')),
                        'time_key_blink':  float(os.environ.get('time_key_blink')),
                        'scroll_time_per_line': float(os.environ.get('scroll_time_per_line')),
                        'fill_sound_list': bool(int(os.environ.get('fill_sound_list'))),
                        'color_assignment': color_assignment,
                        'scroll_amount_lines': int(os.environ.get('scroll_amount_lines')),
                        'folder_for_cutted_songs': os.environ.get('folder_for_cutted_songs'),
                        'progress_bar_update_time': int(os.environ.get('progress_bar_update_time')),
                        'brightness': float(os.environ.get('brightness')),
                        }

    def __oled_startup(self):
        self.__oled = oled_ssd1306(font_size=20)
        self.__oled.clear()
        self.__oled.set_scroll_data(['Die närrischen', 'Herren Buron', 'und', 'Lechtenfeld',
                                   'präsentieren', 'stolz das', 'Soundboard', 'des', 'Freiherren', 'von', 'Kienle'])
        self.__startup_scroll_thread = threading.Thread(target=self.__startup_scroll_data)
        self.__startup_scroll_thread.start()

    def __startup_scroll_data(self):
        self.__oled.show()
        for i in range(0, len(self.__oled.lines) - self.__oled.num_lines_shown):
            time.sleep(1)
            self.__oled.scroll(1)
        self.__oled.clear()
        self.__sound_loading_progress_bar()

    def __sound_loading_progress_bar(self):
        num_sounds = self.__sound_list_object.num_sounds
        while self.__sound_list_object.loaded_sounds != num_sounds:
            percentage = str(round(self.__sound_list_object.loaded_sounds/num_sounds * 100)) + '%'
            line1 = str(self.__sound_list_object.loaded_sounds) + ' / ' + str(num_sounds) + ' ' + percentage
            line2 = 'sounds loaded'
            line3 = self.__progress_bar(self.__sound_list_object.loaded_sounds/num_sounds)
            self.__oled.show([line1, line2, line3 ])
            time.sleep(self.__conf['progress_bar_update_time'])
        self.__oled.clear()
        self.__oled_setup()

    def __progress_bar(self, value):
        line = ''
        value *= 10
        value = round(value)
        for i in range(0, value):
            line += '#'
        for i in range(value, 10):
            line += '-'
        return line

    def __setup(self, folder_path):
        print('Start Setup')
        self.__pin_assignment()
        self.__switches_setup(en_int = False, led_init=True)
        self.__led_setup()
        print('Load Songs')
        self.__sound_list_object = sound_list(self.__conf, folder_path)
        self.__sound_list = self.__sound_list_object.load_sound_list()
        self.__led_matching_sounds = self.__create_led_match_to_sounds(self.__sound_list)
        print('Enable Interrupts')
        self.__switches_setup(en_int = True)

        print('Setup Complete')

    def __runtime(self):

        if self.__conf['switch_simulation']:
            self.switch_simulation = threading.Thread(target=self.__switch_simulation)
            self.switch_simulation.start()
        leds = [x for x in range(0, self.num_pixels)]
        self.__show_leds_layer1()
        print('RUNTIME')
        print()

    def __pin_assignment(self):
        self.__pin_ass = {}
        for i in range(2,28):
            self.__pin_ass[i] = os.environ.get(str(i))
            if '_' in self.__pin_ass[i]:
                [mode, value] = self.__pin_ass[i].split('_')
                if 'F' in mode:
                    self.extra_function_key = i
        try:
            self.extra_function_key
        except NameError:
            self.extra_function_key = None
        
    def __led_setup(self):
        self.__led.set_color_samples(self.__conf['color_assignment'])
        leds = ['boot_loading_color' for x in range(0, self.num_pixels)]
        self.__led.show_color_samples_from_list(leds)
    
    def __switches_setup(self, en_int=True, led_init=False):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        for pin, value in self.__pin_ass.items():
            
            if (value == 'LED') or (value == 'OLED') or (value == ''):
                if value == 'LED' and led_init:
                    self.__led = led_pixels(led_pin(pin), self.num_pixels, self.__conf['brightness'], enabled=self.__conf['led_enabled'])
                continue
            [mode, pin_value] = value.split('_')
            if 'I' in mode:
                extra_func = None
                if pin_value == 'u' or pin_value == 'd':
                    self.__key_arrows[pin_value] = pin
                    self.__gpio_in_setup(pin)
                    extra_func = pin_value
                else:
                    pin_value = int(pin_value,  16)
                    self.__gpio_in_setup(pin)
                if en_int:
                    self.__gpio_in_en_int(pin, extra_func=extra_func)
            else:
                pin_value = int(pin_value, 16)
                self.__gpio_out_setup(pin)
        
    def __gpio_in_setup(self, pin):
        GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    
    def __gpio_in_en_int(self, pin, extra_func=None):
        GPIO.add_event_detect(pin, GPIO.FALLING, callback=self.__key_interrupt, bouncetime=self.__conf['debounce_time'])
        
    def __gpio_out_setup(self, pin, init=0):
        GPIO.setup(pin, GPIO.OUT, initial=init)
        
    def __key_interrupt(self, pin):
        print()
        print('Key interrupt')
        print(time.time())
        [temp, key] = self.__pin_ass[pin].split('_')
        print("key_int: " + str(key))
        if key == 'u' or key == 'd':
            self.__menu_key_interrupt(pin)
        else:
            key = int(key, 16)
            self.__generell_key_int(key)
        print('EndISR: ', time.time())

    def __generell_key_int(self, key):
        if self.__time + self.__conf['time_to_select'] > time.time():
            sound_id = self.__conf['amount_select_keys'] * self.__key1 + key
            self.__time = 0
            self.__show_leds_layer1()
            self.__play(sound_id)
        else:
            self.__key1 = key
            self.__show_leds_layer2(key)
            self.__fade_to_layer1_thread = threading.Thread(target=self.__show_leds_layer1_after_time, args=([self.__conf['time_to_select']]))
            self.__fade_to_layer1_thread.start()
            self.__time = time.time()
            self.__oled.set_cursor_row(self.__get_row_from_key(self.__key1))

    def __get_row_from_key(self, key_layer1):
        line_count = 0
        for item in self.__sound_list:
            if item['sound_id'] >= key_layer1 * 16:
                break
            line_count += 1
        return line_count

    def __menu_key_interrupt(self, pin):
        print('Menu interrupt')
        print(time.time())
        starttime = time.time()
        [temp, key] = self.__pin_ass[pin].split('_')
        if self.extra_function_key !=None:
            n_extra_f = GPIO.input(self.extra_function_key)
        else:
            n_extra_f = 0
        if n_extra_f:
            print(key)
            if key == 'u':
                direction = -self.__conf['scroll_amount_lines']
                key = 16
            elif key == 'd':
                direction = self.__conf['scroll_amount_lines']
                key = 17
            else:
                direction = 0
            self.__oled.scroll(direction)
            #print(time.time())
            self.__scroll_thread = threading.Thread(target=self.__scroll, args=([pin, direction]))
            #print(time.time())
            self.__scroll_thread.start()
            #print(time.time())
        else:
            print("EXTRAFUNCTION")
            if key == 'd':
                start_time = time.time()
                time_to_shutdown = 5
                while not(GPIO.input(pin)):
                    if start_time+time_to_shutdown < time.time():
                        self.__oled.clear()
                        self.__oled.show(['SHUTDOWN', 'NOW'])
                        shutdown()
                        time.sleep(5)
                    self.__oled.clear()
                    self.__oled.show(['SHUTDOWN ', 'in ', str(start_time+time_to_shutdown-time.time())+'s'])
                    time.sleep(0.1)

                self.__oled.clear()
            if key == 'u':
                start_time = time.time()
                time_to_shutdown = 5
                while not(GPIO.input(pin)):
                    if start_time+time_to_shutdown < time.time():
                        self.__oled.clear()
                        self.__oled.show(['UPDATE', 'NOW'])
                        self.update()
                        reboot()
                        time.sleep(3)
                    self.__oled.clear()
                    self.__oled.show(['UPDATE ', 'in ', str(start_time + time_to_shutdown - time.time()) + 's'])
                    time.sleep(0.1)

    def update(self):
        main_path = os.path.abspath(os.path.dirname(sys.argv[0]))
        path_split = main_path.split('/')
        folder = '/'.join(path_split[:-1])
        action = ' '.join(['cd', folder, '&& git pull'])
        answer = os.system(action)


    def __scroll(self, pin, direction):
        print('scroll thread')
        time.sleep(self.__conf['scroll_time_per_line'])
        while not GPIO.input(pin):
            print('scroll_one_line')
            self.__oled.scroll(direction)
            time.sleep(self.__conf['scroll_time_per_line'])
        print('scroll thread ENDE')

    def __create_led_match_to_sounds(self, sound_list):
        led_matching_sounds_layer2_int, led_matching_sounds_color_layer2 = self.__create_led_matching_to_sounds_layer2(sound_list)
        led_matching_sounds_layer1_color = self.__count_sounds_per_key_in_first_layer(led_matching_sounds_layer2_int)
        return [led_matching_sounds_layer1_color, led_matching_sounds_color_layer2]

    def __create_led_matching_to_sounds_layer2(self, sound_list):
        led_matching_sounds_layer2_int = []
        led_matching_sounds_color_layer2 = []
        for i in range(0, self.__conf['amount_select_keys']*self.__conf['amount_select_keys']):
            item = next((item for item in sound_list if item['sound_id'] == i), None)
            led_color = self.__convert_item_to_color_layer2(item)
            led_matching_sounds_color_layer2.append(led_color)
            led_value = self.__convert_item_to_layer2_int(item)
            led_matching_sounds_layer2_int.append(led_value)
        return led_matching_sounds_layer2_int, led_matching_sounds_color_layer2

    def __count_sounds_per_key_in_first_layer(self, led_matching_sounds_layer2_int):
        led_matching_sounds_layer1_color = []
        for i1 in range(0, self.__conf['amount_select_keys']):
            counter_for_key = 0
            for i2 in range(0, self.__conf['amount_select_keys']):
                counter_for_key += led_matching_sounds_layer2_int[i1 * self.__conf['amount_select_keys'] + i2]
            led_color = self.__convert_int_to_color_layer1(counter_for_key)
            led_matching_sounds_layer1_color.append(led_color)
        return led_matching_sounds_layer1_color

    def __convert_int_to_color_layer1(self, int_value):
        if int_value == 0:
            return 'no_sounds_color'
        #this minus one is considered because its hard to have song's on the double number id's( e.g. 00, 11, etc.)
        #so now all sounds is considered not 16 but 15 keys
        elif int_value < (self.__conf['amount_select_keys'] - 1):
            return 'some_sounds_color'
        else:
            return 'all_sounds_color'

    def __convert_item_to_color_layer2(self, item):
        if item == None:
            return 'no_sounds_color'
        else:
            return 'play_sound_color'
    
    def __convert_item_to_layer2_int(self, item):
        if item == None:
            return 0
        else:
            return 1

    def __show_leds_layer1(self):
        leds = list(self.__led_matching_sounds[0])
        leds.append('arrow_color')
        leds.append('arrow_color')
        print(leds)
        self.__led.show_color_samples_from_list(leds)

    def __show_leds_layer1_after_time(self, after_time):
        print('show_leds_layer1_after_time start')
        print(time.thread_time())
        self.__timer(after_time)
        self.__show_leds_layer1()
        print('show_leds_layer1_after_time end')
        print(time.thread_time())

    def __timer(self, sleep_time, start_time=time.thread_time()):
        target_time = start_time + sleep_time
        while time.thread_time() < target_time:
            pass

    def __show_leds_layer2(self, key):
        all_leds = self.__led_matching_sounds[1]
        start_led = key * self.__conf['amount_select_keys']
        end_led = (key + 1) * self.__conf['amount_select_keys']
        leds = all_leds[start_led:end_led]
        print(len(leds))
        self.__led.show_color_samples_from_list(leds)

    def __play(self, sound_id):
        print('sound :' + str(sound_id))
        sound_entry = next((item for item in self.__sound_list if item['sound_id'] == sound_id), None)
        print(sound_entry)
        if sound_entry == None:
            raise Exception(f'No sound bind to this key')

        if self.__conf['parallel_sound']:
            self.musicplayer_thread = threading.Thread(target=self.__play_thread, args=(sound_entry,))
            self.musicplayer_thread.start()
        else:
            self.__play_thread(sound_entry)

    def __play_thread(self, sound_entry):
        print('Music thread Start')
        play(sound_entry['sound'])
        print('Music thread Ende')
        
    def __oled_setup(self):
        scroll_list = []
        for sound in self.__sound_list:
            if sound['name']!='':
                hex_code = (sound['filename'].split('_'))[0]
                name = sound['name']
                text = str(hex_code) + ' | ' + name
                scroll_list.append(text)
            else:
                placeholder = sound['sound_id'] + '|'
                scroll_list.append(placeholder)
        self.__oled.set_scroll_data(scroll_list)

    def __switch_simulation(self):
        while True:
            key = input()
                
            if key == 'u':
                self.__oled.scroll(-1)
            elif key == 'j':
                self.__oled.scroll(1)
            else:
                value = int(key, 16)
                self.__generell_key_int(value)
            


if __name__ == "__main__":
    sb = soundboard('/home/pi/Music/')
    while True:
        pass

