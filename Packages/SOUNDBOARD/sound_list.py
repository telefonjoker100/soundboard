import os
import glob
from pydub import AudioSegment

class sound_list():
    def __init__(self, config, folder_path):
        self.__conf = config
        self.folder_path = folder_path
        self.loaded_sounds = 0
        self.num_sounds = self.__get_num_sounds()

    def __get_num_sounds(self):
        num_sounds = 0
        os.chdir(self.folder_path)
        for extension in self.__conf['extension_list']:
            for sound_in_dir in glob.glob(extension):
                num_sounds += 1
        return num_sounds

    def load_sound_list(self, ):
        sound_list = self.__sound_list_create(self.folder_path)
        if self.__conf['fill_sound_list']:
            sound_list = self.__fill_sound_list(sound_list)
        return sound_list

    def __sound_list_create(self, folder_path):
        sound_list = []
        os.chdir(folder_path)
        for extension in self.__conf['extension_list']:
            sound_list = self.__load_all_sounds_from_extension(sound_list, extension)
        sound_list = sorted(sound_list, key=lambda item: item['sound_id'])
        return sound_list

    def __load_all_sounds_from_extension(self, sound_list, extension):
        for sound_in_dir in glob.glob(extension):
            data = self.__load_sound_from_sound_in_dir(sound_in_dir, extension)
            if data == -1:
                continue
            self.loaded_sounds += 1
            sound_list.append(data)
        return sound_list

    def __load_sound_from_sound_in_dir(self, sound_in_dir, extension):
        filename = self.__get_filename_from_sound_in_dir(sound_in_dir)
        sound = self.__load_sound(sound_in_dir, extension)
        sound_id = self.__get_sound_id_from_filename(filename)
        name = self.__get_name_for_sound(filename)
        # skip element if name or id not satisfying
        if len(sound_id) != 4 or len(name) <= 0:
            return -1
        print(name)
        return {'filename': filename, 'name': name, 'sound_id': int(sound_id, 16), 'sound': sound}

    def __load_sound(self, sound_in_dir, extension):
        filename = self.__get_filename_from_sound_in_dir(sound_in_dir)
        if self.__check_if_song_already_cutted(filename, extension):
            sound = self.__load_sound_from_cutted_sounds_folder(filename, extension)
        else:
            sound = self.__load_sound_from_sound_dir(sound_in_dir)
            if self.__check_if_time_stamp_in_name(filename):
                sound = self.__cut_sound_to_timestamps(sound, filename)
                if sound == -1:
                    return -1
                self.__safe_cutted_sound(sound, filename, extension)

        sound = self.__add_volume_to_sound(sound, filename)
        return sound

    def __get_filename_from_sound_in_dir(self, sound_in_dir):
        return os.path.splitext(os.path.basename(sound_in_dir))[0]

    def __get_filename_split_from_filename(self, filename):
        return filename.split('_')

    def __get_sound_id_from_filename(self, filename):
        return '0x' + self.__get_filename_split_from_filename(filename)[0]

    def __get_volume_from_filename(self, filename):
        return int(self.__get_filename_split_from_filename(filename)[1])

    def __check_if_time_stamp_in_name(self, filename):
        filename_split = filename.split('_')
        if '.' in filename_split[2]:
            return True
        else:
            return False

    def __load_sound_from_sound_dir(self, sound_in_dir):
        return AudioSegment.from_file(sound_in_dir)

    def __load_sound_from_cutted_sounds_folder(self, filename, extension):
        return AudioSegment.from_file(self.__get_file_path_for_cutted_songs(filename, extension))

    def __get_name_for_sound(self, filename):
        filename_split = filename.split('_')
        if self.__check_if_time_stamp_in_name(filename):
            name = ' '.join(filename_split[3:])
        else:
            name = ' '.join(filename_split[2:])
        return name

    def __check_if_song_already_cutted(self, filename, extension):
        if not self.__check_if_folder_cutted_songs_exist():
            self.__create_folder_cutted_songs()
        file_path = self.__get_file_path_for_cutted_songs(filename, extension)
        if os.path.isfile(file_path):
            return True
        else:
            return False

    def __add_volume_to_sound(self, sound, filename):
        return sound + self.__get_volume_from_filename(filename)

    def __get_file_path_for_cutted_songs(self, filename, extension):
        file_folder_path = self.__conf['folder_for_cutted_songs']
        filename_with_extension = filename + extension[1:]
        file_path = os.path.join(file_folder_path, filename_with_extension)
        return file_path

    def __check_if_folder_cutted_songs_exist(self):
        folder_path = self.__conf['folder_for_cutted_songs']
        if os.path.isdir(folder_path):
            return True
        else:
            return False

    def __create_folder_cutted_songs(self):
        folder_path = self.__conf['folder_for_cutted_songs']
        os.system('mkdir ' + folder_path)

    def __cut_sound_to_timestamps(self, sound, filename):
        filename_split = self.__get_filename_split_from_filename(filename)
        [starttime_str, endtime_str] = filename_split[2].split('-')
        starttime = self.__time_str_to_int(starttime_str)
        endtime = self.__time_str_to_int(endtime_str)
        if starttime == endtime:
            return -1
        else:
            sound = sound[starttime:endtime]
            return sound

    def __safe_cutted_sound(self, sound: AudioSegment, filename, extension):
        file_path = self.__get_file_path_for_cutted_songs(filename, extension)
        sound.export(file_path, extension[2:])

    def __get_sound_filename_extension(self, filename):
        return filename.split('.')[-1:]

    def __fill_sound_list(self, sound_list):
        max_id = self.__get_max_id_of_soundlist(sound_list)
        filled_sound_list = self.__fill_placeholders_between_songs(sound_list)
        filled_sound_list = self.__fill_placeholders_after_songs(filled_sound_list, max_id)
        filled_sound_list = self.__replace_placeholders_with_sound_id(filled_sound_list)
        return filled_sound_list

    def __get_max_id_of_soundlist(self, sound_list):
        id_list = [sound['sound_id'] for sound in sound_list]
        max_id = max(id_list)
        return max_id

    def __fill_placeholders_between_songs(self, sound_list):
        filled_sound_list = []
        for sound in sound_list:
            # fill gaps between numbers with empty array
            while sound['sound_id'] != len(filled_sound_list):
                filled_sound_list.append([])
            # insert sound
            filled_sound_list.append(sound)
        return filled_sound_list

        # fill rest with empty arrays

    def __fill_placeholders_after_songs(self, filled_sound_list, max_id):
        for i in range(max_id + 1, self.__conf['amount_select_keys'] * self.__conf['amount_select_keys']):
            filled_sound_list.append([])
        return filled_sound_list

    def __replace_placeholders_with_sound_id(self, filled_sound_list):
        for i in range(0, len(filled_sound_list)):
            if len(filled_sound_list[i]) == 0:
                filled_sound_list[i] = {'name': '', 'sound_id': self.__int_to_hex_upper(i)}
        return filled_sound_list

    def __int_to_hex_upper(self, integer):
        text_hex_full = str(hex(integer))
        [part_0x, text_hex_number] = text_hex_full.split('x')
        if len(text_hex_number) == 1:
            text_hex_number = '0' + text_hex_number
        text_hex_upper = text_hex_number.upper()
        return text_hex_upper

    def __time_str_to_int(self, time_str):
        time_array = time_str.split('.')
        if time_array == ['']:
            timestamp = None
        elif len(time_array) == 2:
            timestamp = (int(time_array[0]) * 60 + int(time_array[1])) * 1000
        elif len(time_array) == 3:
            timestamp = (int(time_array[0]) * 60 + int(time_array[1])) * 1000 + int(time_array[2])
        else:
            assert False, 'WRONG TIME STEPS'
        return timestamp