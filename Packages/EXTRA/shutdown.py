import os
import RPi.GPIO as GPIO

def reboot():
    GPIO.cleanup()
    print('reboot')
    os.system('sudo reboot')
    
def shutdown():
    GPIO.setmode(GPIO.BCM)
    GPIO.cleanup()
    print('shutdown')
    os.system('sudo shutdown now')