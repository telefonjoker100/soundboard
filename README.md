# Soundboard

RPI-Soundboard with LED(WS2812B) Feedback and an OLED ssd1306. 


INSTALLATION:

MAKE SURE TO ENABLE VNC, I2C and SSH

Go into `/boot/config.txt` and add following parameter:

```
hdmi_ignore_edid_audio=1
```

Now we have to make it run on boot-up:
The only audio-support boot-up option i have found is .desktop application

```
cd ~/.config
mkdir autostart
sudo nano soundboard.desktop
```
Enter following code in there and modify the path to your own folder
```
[Desktop App]
Name=soundboard
Type=Application
Exec=python /home/pi/soundboard/run.py
```

