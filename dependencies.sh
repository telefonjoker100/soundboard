#!/bin/bash
sudo pip3 install python-dotenv
sudo pip3 install adafruit-board-toolkit
sudo pip3 install pydub
sudo pip3 install luma.core
sudo pip3 install luma.oled
sudo pip3 install adafruit-circuitpython-neopixel
sudo pip3 install rpi_ws281x
sudo pip3 install Adafruit-Blinka
